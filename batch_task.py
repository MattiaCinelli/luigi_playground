import datetime
import boto3

from isoma.pipeline.inivata_framework.log_operations import get_luigi_logger
from isoma.pipeline.inivata_framework.luigi_operations import fail_if_outputs_not_created
from luigi.contrib.batch import BatchTask

from isoma.pipeline.base_pipeline_tasks.utils import dict_with
from isoma.pipeline.inivata_framework.core_param_mixin import CoreParamMixin

import os


class AbstractInivataBatchTask(CoreParamMixin, BatchTask):
    def __init__(self, **kwargs):
        super(AbstractInivataBatchTask, self).__init__(
            **dict_with(kwargs, {
                'parallel_env': 'smp',
                'no_tarball': True,
            }))

    @fail_if_outputs_not_created
    def run(self):
        super(AbstractInivataBatchTask, self).run()

    # SGE stores the stdout and stderr of an batch job on the head node. Luigi deletes these if the job is successful
    # We cannot modify this part of luigi without forking the code
    # We override _track_job to add a section that will log stdout and stderr before they get deleted.
    # This method uses get_luigi_logger() as we are currently running on the head node so the luigi-interface logger
    # is available
    def _track_job(self):
        super(AbstractInivataBatchTask, self)._track_job()

        get_luigi_logger().info('Task of class {0} and Batch job_id {1} finished running on Batch at {2}:\nstdout was :\n{3}\nstderr was:\n{4}\n'.format(
            self.__class__.__name__,
            self.job_id,
            datetime.datetime.now(),
            self._get_messages_as_display_string(self.outfile),
            self._get_messages_as_display_string(self.errfile)
        ))

    def _get_messages_as_display_string(self, batch_logfile):
        messages = self._get_batch_log_messages(batch_logfile)

        return '/n'.join(messages)

    @staticmethod
    def _get_batch_log_messages(batch_logfile):
        if not os.path.exists(batch_logfile):
            get_luigi_logger().info('No error file')
            return []
        with open(batch_logfile, "r") as f:
            messages = f.readlines()
        if messages == []:
            return messages
        if messages[0].strip() == 'stdin: is not a tty':  # batch complains when we submit through a pipe
            messages.pop(0)
        return messages

