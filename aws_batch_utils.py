#!/usr/bin/env python
import os
import sys
import boto3
from time import sleep

def get_batch_logs(logStreamName):
    '''Returns log_event dict for logStreamName.'''
    
    # Example: logStreamName = 'ata-job-def-001/default/98bb16e6-737a-45fe-9caa-78e8646a6d38'
    log_client = boto3.client('logs')

    response = log_client.get_log_events(
        logGroupName='/aws/batch/job',
        logStreamName=logStreamName
    )
    return response

def process_batch_logs(describe_jobs_response, verbose=False):
    '''Returns array of error messages for describe_jobs response dict.'''

    messages = []
    for job in describe_jobs_response['jobs']:
        logStreamName = job['container']['logStreamName']
        response = get_batch_logs(logStreamName)
        for event in response['events']:
            messages.append(event['message'])
    if verbose:
        print "Messages: %s" % ("".join(messages))
    return messages


def wait_batch_jobId(jobId, client = None, verbose = False):
    '''Function takes AWS Batch jobId and returns job status once it has become either 'SUCCEEDED' or 'FAILED'.'''

    if client is None:
        client = boto3.client('batch')

    # check job status every 5 seconds until job has either 'SUCCEEDED' or 'FAILED'
    # AWS Batch job status will always be one of the following 'SUBMITTED'|'PENDING'|'RUNNABLE'|'STARTING'|'RUNNING'|'SUCCEEDED'|'FAILED'
    while True:
        r = client.describe_jobs(jobs=[jobId,])
        status = r['jobs'][0]['status']
        if verbose:
            print "Jobid=\'%s\', Status=\'%s\'" % (jobId, status)
            print r
            print "\n"
        if status in ('SUCCEEDED'):
            break
        if status in ('FAILED'):
            process_batch_logs(r, verbose)
            break
        sleep(5)

    return status


def wait_batch_job(job_response, client = None, verbose=False):
    '''Function takes AWS Batch response dict from client.submit_job and returns job status once it has become either 'SUCCEEDED' or 'FAILED'.'''

    jobId = response['jobId']

    return wait_batch_jobId(jobId, client=client, verbose=verbose)

