import os
import sys
import luigi

class submit_job_luigi(luigi.Task):
    def run(self):
        return(os.system('python /shared/margus_mattiadebug/register_and_submit.py'))

class aws_sub_luigi(luigi.Task):
    def requires(self):
        return(submit_job_luigi())
    
    
if __name__ == '__main__':
    luigi.run()
