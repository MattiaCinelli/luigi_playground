#!/usr/bin/env python
import boto3
import os

client = boto3.client('batch', region_name='eu-west-1')

def register_aws_job():
    response = client.register_job_definition(
        jobDefinitionName='mattia-due-jobd',
        type='container',    
        containerProperties={
            'image': '656643668052.dkr.ecr.eu-west-1.amazonaws.com/mattia-uno:latest',
            'vcpus': 1,
            'memory': 1024,
            'jobRoleArn': 'arn:aws:iam::656643668052:role/mattia-batch-src',
        },
        retryStrategy={
            'attempts': 1
        },
        timeout={
            'attemptDurationSeconds': 120
        }
    )
    print(response)

def submit_aws_job():
    response = client.submit_job(
        jobName="margus_mattia_debug_code_1",
        jobQueue='mattia-cmd-q',
        jobDefinition='mattia-due-jobd',
        containerOverrides={
            'environment': [
                {'name':'RUNID','value':'WWTP'},
                {'name':'DMPATH','value':'/shared/tmp/'},
                {'name':'AWS_ACCESS_KEY_ID','value':'AKIAJ252DOBDSBAA3LYA'},
                {'name':'AWS_SECRET_ACCESS_KEY','value':'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'},
                {'name':'AWS_DEFAULT_REGION','value':'eu-west-1'}]})
#        timeout={'attemptDurationSeconds': 240})

    print(response)

submit_aws_job()
